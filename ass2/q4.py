from __future__ import print_function, division
import data
import numpy as np
import random
import common

np.set_printoptions(threshold=np.nan)

def logsumexp(E, axis=None):
  '''Take vector of arguments in log space. Compute the sum of exp(E), with
  return value back in log space.'''
  A = np.max(E, axis=axis)
  E -= A
  E[E < -50] = -50
  return A + np.log(np.sum(np.exp(E), axis=axis))

def calc_llh(X, pi, theta):
  llh = 0
  for x in X:
    pxc = np.log(pi) + np.sum(np.log(theta**x), axis=1) + np.sum(np.log((1 - theta)**(1 - x)), axis=1)
    llh += logsumexp(pxc)
  return llh

def sigmoid(x):
  return 1 / (1 + np.exp(-x))

def logsigmoid(x):
  return -np.log(1 + np.exp(-x))

def make_minibatch(X, size):
  N, _ = X.shape
  idxs = np.arange(N)
  np.random.shuffle(idxs)
  idxs = idxs[:size]
  return X[idxs,:]

def do_em(X, pi, iterations):
  N, D = X.shape
  K = len(pi)
  Z = np.zeros((N, K))
  norm = np.zeros(K)
  #theta = np.random.uniform(size=(K, D))
  theta = np.loadtxt('q4_theta.txt')

  for iteration in range(iterations):
    # E step
    for i in range(N):
      Z[i,:] = pi * np.prod(theta**X[i], axis=1) * np.prod((1 - theta)**(1 - X[i]), axis=1)
      S = np.sum(Z[i,:])
      if S > 0:
        Z[i,:] /= S
      else:
        Z[i,:] = 1./K

    # M step
    ZTX = np.dot(Z.T, X) # KxD
    for k in range(K):
      norm[k] = np.sum(Z[:,k])
      theta[k,:] = ZTX[k] / norm[k]

    print(iteration, 'llh', calc_llh(X, pi, theta))
  return theta

def sample_bottoms(images, theta, pi):
  to_sample = 20
  K, D = theta.shape

  assert D % 2 == 0
  mid = int(D/2)

  tops = np.random.permutation(images)[:to_sample]

  # Ensure bottom is blank.
  for idx in range(len(tops)):
    tops[idx][mid:] = 0.

  marginals = []
  for x in tops:
    # p(x_top | pi, theta)
    Z = pi * np.prod(theta[:,:mid]**x[:mid], axis=1) * np.prod((1 - theta[:,:mid])**(1 - x[:mid]), axis=1)

    M = np.copy(x).astype(np.float64)
    for d in range(mid, D):
      pixel = theta[:,d]**x[d] * (1 - theta[:,d])**(1 - x[d])
      M[d] = 1 - np.sum(pixel * Z) / np.sum(Z)
    marginals.append(M)

  data.save_images(np.array(marginals), 'q4_bottom_marginals.png')

def main():
  N_data, train_images, train_labels, test_images, test_labels = data.load_mnist()
  train_images = (train_images > 0.5).astype(np.uint8)
  limit = int(1e3)
  limit = None
  train_images, train_labels = train_images[:limit], train_labels[:limit]
  #np.random.seed(1)

  K = 30
  pi = np.ones(K) / K

  theta = np.loadtxt('q4_theta.txt')
  sample_bottoms(train_images, theta, pi)
  return

  theta = do_em(train_images, pi, 100)
  data.save_images(theta, 'q4_theta.png')
  np.savetxt('q4_theta.txt', theta)

  sample_bottoms(train_images, theta, pi)


main()
