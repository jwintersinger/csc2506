from __future__ import print_function, division
import data
import numpy as np
import random
import common

np.set_printoptions(threshold=np.nan)

def make_minibatch(imgs, labels, size):
  assert len(imgs) == len(labels)
  N, _ = imgs.shape
  idxs = np.arange(N)
  np.random.shuffle(idxs)
  idxs = idxs[:size]
  return (imgs[idxs,:], labels[idxs,:])

def calc_llh_for_class(X, W, C, c):
  WXt = np.dot(W, X.T)
  # Find max in each column: Nx1
  B = np.max(WXt, axis=0)
  expWXt = np.exp(WXt - np.tile(B, (C, 1)))
  logsum = np.log(np.sum(expWXt, axis=0)) + B # Nx1

  wX = np.dot(X, W[c]) # Nx1
  wX -= logsum
  return wX

def calc_llh(W, imgs, labels):
  parted = common.partition_images(imgs, labels)
  C = len(parted)

  llh = 0
  for c, X in parted.items():
    llh += np.sum(calc_llh_for_class(X, W, C, c))
  return llh

def grad_desc(imgs, labels):
  N, D = imgs.shape
  _, C = labels.shape
  W = np.random.normal(size=(C, D))
  W = np.zeros((C, D))
  batch_size = 100
  learn_rate = 0.5
  iterations = 1000

  last_llh = -float('inf')
  last_W = None

  for I in range(iterations):
    mb_imgs, mb_labels = make_minibatch(imgs, labels, batch_size)
    N_batch, _ = mb_imgs.shape
    parted = common.partition_images(mb_imgs, mb_labels)

    grad = np.zeros(W.shape)
    for c, X in parted.items():
      for x in X:
        Wx = np.dot(W, x)
        b = np.max(Wx)
        logsum = b + np.log(np.sum(np.exp(Wx - b)))
        sumc = np.exp(logsum)

        for k in range(C):
          if k == c:
            grad_wk = x * (1 - (np.exp(Wx[k]) / sumc))
          else:
            grad_wk = -x * np.exp(Wx[k]) / sumc
          grad[k,:] += grad_wk

    grad /= N_batch
    W += learn_rate * grad

    llh = calc_llh(W, imgs, labels)
    if llh > last_llh:
      print('%s\tkeeping\t%.2e\t%.2e' % (I, learn_rate, llh))
      last_llh = llh
      last_W = W
      learn_rate *= 1.1
    else:
      print('%s\trevert\t%.2e\t%.2e' % (I, learn_rate, llh))
      llh = last_llh
      W = last_W
      learn_rate *= 0.5

  return W

def classify(X, C, W):
  N, D = X.shape
  cllh = np.zeros((N, C))
  for c in range(C):
    cllh[:,c] = calc_llh_for_class(X, W, C, c)
  return np.argmax(cllh, axis=1)

def softmax(X):
  b = np.max(X)
  logS = X - (b + np.log(np.sum(np.exp(X - b))))
  return np.exp(logS)

def q3(train_images, train_labels, test_images, test_labels):
  W = grad_desc(train_images, train_labels)
  _, C = train_labels.shape

  transformed_W = np.copy(W)
  softmax_W = np.copy(W)
  for c in range(C):
    maxw = np.max(np.abs(transformed_W[c,:]))
    transformed_W[c,:] = (transformed_W[c,:] + maxw) / (2*maxw)
    softmax_W[c,:] = softmax(softmax_W[c,:])
    assert 0 <= np.min(transformed_W[c,:]) <= np.max(transformed_W[c,:]) <= 1
  data.save_images(W, 'weights.orig.png')
  data.save_images(transformed_W, 'weights.transformed.png')
  data.save_images(softmax_W, 'weights.softmax.png')

  for dataset, X, L in (('Training', train_images, train_labels), ('Test', test_images, test_labels)):
    N, D = X.shape
    llh = calc_llh(W, X, L)
    classes = np.argmax(L, axis=1)
    preds = classify(X, C, W)
    print(dataset, 'accuracy: %.2f' % (np.sum(preds == classes) / N))
    print(dataset, 'mean predictive LLH: %.2f' % (llh / N))

def main():
  N_data, train_images, train_labels, test_images, test_labels = data.load_mnist()
  limit = int(1e2)
  limit = None
  train_images, train_labels = train_images[:limit], train_labels[:limit]
  q3(train_images, train_labels, test_images, test_labels)

main()
