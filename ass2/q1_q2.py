import data
import numpy as np
import random
import common

np.set_printoptions(threshold=np.nan)

def map_theta(X, L):
  _, D = X.shape
  partitioned = common.partition_images(X, L)
  C = len(partitioned)
  theta = np.zeros((C, D))

  for c, partitioned in partitioned.items():
    N, _ = partitioned.shape
    theta[c,:] = (1 + np.sum(partitioned, axis=0)) / (2 + N)

  return theta

def calc_norm(X, pi, theta):
  C, _ = theta.shape
  N, D = X.shape

  lognorm = np.zeros((N, C))
  for c in range(C):
    # px = p(x, c | theta, pi)
    px = np.dot(X, np.log(theta[c])) + np.dot(1 - X, np.log(1 - theta[c])) # Nx1
    px += np.log(pi[c])
    lognorm[:,c] = px

  return np.sum(np.exp(lognorm), axis=1) # Nx1

def calc_predllh(X, classes, pi, theta):
  L = []
  for x, c in zip(X, classes):
    llh = 0
    llh += np.log(pi[c])
    llh += np.dot(x, np.log(theta[c])) + np.dot(1 - x, np.log(1 - theta[c]))
    L.append(llh)
  L = np.array(L) - np.log(calc_norm(X, pi, theta))
  return L

def classify(X, nclasses, pi, theta):
  N, D = X.shape
  preds = np.zeros((N, nclasses))
  for c in range(nclasses):
    classes = np.zeros(N, dtype=np.int) + c
    preds[:,c] = calc_predllh(X, classes, pi, theta)
  return np.argmax(preds, axis=1)

def q1(train_images, train_labels, test_images, test_labels):
  theta = map_theta(train_images, train_labels)
  data.save_images(theta, 'map_theta.png')
  C, D = theta.shape
  pi = np.ones(C) / C

  for dataset, X, L in (('Training', train_images, train_labels), ('Test', test_images, test_labels)):
    classes = np.argmax(L, axis=1)
    predllh = calc_predllh(X, classes, pi, theta)
    preds = classify(X, C, pi, theta)
    N, _ = X.shape
    print(dataset, 'accuracy: %.2f' % (np.sum(preds == classes) / N))
    print(dataset, 'mean predictive LLH: %.2f' % np.mean(predllh))

def sample_joint(theta):
  C, D = theta.shape
  sample_size = 10
  samples = []

  #for c in np.random.choice(range(C), size=sample_size):
  for c in range(C):
    unif = np.random.uniform(size=D)
    x = (unif < theta[c]).astype(np.int)
    samples.append(x)

  samples = np.array(samples)
  data.save_images(samples, 'samples.png')

def sample_bottoms(images, labels, theta, pi):
  parted = common.partition_images(images, labels)
  tops = []
  to_sample = 2
  classes = sorted(parted.keys())

  D = len(parted[classes[0]][0])
  assert D % 2 == 0
  mid = int(D/2)

  for c in classes:
    E = list(parted[c])
    random.shuffle(E)
    tops += E[:to_sample]

  # Ensure bottom is blank.
  for idx in range(len(tops)):
    tops[idx][mid:] = 0.

  marginals = []
  for x in tops:
    # p(x_top | pi, theta)
    pxtc = 0
    for c in classes:
      logpxtc = np.log(pi[c])
      logpxtc += np.dot(x[:mid], np.log(theta[c,:mid])) + np.dot(1 - x[:mid], np.log(1 - theta[c,:mid])) # Nx1
      pxtc += np.exp(logpxtc)

    M = np.copy(x)
    for d in range(mid, D):
      # p(x_i_in_bottom | x_top, pi, theta)
      pxbd = 0
      for c in classes:
        # p(c | theta)
        logpxbdc = np.log(pi[c])
        # p(x_top | c, theta)
        logpxbdc += np.dot(x[:mid], np.log(theta[c,:mid])) + np.dot(1 - x[:mid], np.log(1 - theta[c,:mid]))
        # p(x_d | c, theta)
        logpxbdc += x[d]*np.log(theta[c,d]) + (1 - x[d])*np.log(1 - theta[c,d])
        pxbd += np.exp(logpxbdc) / pxtc
      M[d] = 1 - pxbd
    marginals.append(M)

  data.save_images(np.array(marginals), 'bottom_marginals.png')

def q2(train_images, train_labels, test_images, test_labels):
  theta = map_theta(train_images, train_labels)
  C, D = theta.shape
  pi = np.ones(C) / C
  sample_joint(theta)
  sample_bottoms(train_images, train_labels, theta, pi)

def main():
  N_data, train_images, train_labels, test_images, test_labels = data.load_mnist()
  limit = int(1e2)
  limit = None
  train_images, train_labels = train_images[:limit], train_labels[:limit]

  q1(train_images, train_labels, test_images, test_labels)
  q2(train_images, train_labels, test_images, test_labels)

main()
