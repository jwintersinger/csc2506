from __future__ import print_function, division
import data
import common
import os
import sys

import autograd.numpy as np
import autograd
import autograd.optimizers

#from numba import jit

np.seterr(over='raise')

def make_minibatch(imgs, labels, size):
  assert len(imgs) == len(labels)
  N, _ = imgs.shape
  idxs = np.arange(N)
  np.random.shuffle(idxs)
  idxs = idxs[:size]
  return (imgs[idxs,:], labels[idxs,:])

def norm_logpdf(x, mu, sigma_sq):
  return -np.log(np.sqrt(2 * np.pi * sigma_sq)) - ((x - mu)**2 / (2*sigma_sq))

def q_expectation_monte_carlo(variational_mu, variational_psi, X, T, prior_sigma_sq, nsamples):
  assert variational_mu.shape == variational_psi.shape
  C, D = variational_mu.shape
  N, _ = X.shape

  softmax_num = 0
  softmax_denom = 0
  prior = 0
  entropy = 0

  for j in range(nsamples):
    W = (np.random.standard_normal(size=variational_mu.shape) * np.sqrt(np.exp(variational_psi))) + variational_mu
    XWt = np.dot(X, W.T) # NxC

    softmax_num   = softmax_num + np.sum(XWt[range(N),T])
    B = np.max(XWt, axis=1)
    softmax_denom = softmax_denom - np.sum(B + np.log(np.sum(np.exp(XWt - B[:,np.newaxis]), axis=1)))

    prior   = prior   + (np.sum(norm_logpdf(x=W, mu=0, sigma_sq=prior_sigma_sq)))
    entropy = entropy - np.sum(norm_logpdf(x=W, mu=variational_mu, sigma_sq=np.exp(variational_psi)))

  return (softmax_num + softmax_denom + prior + entropy) / nsamples

# Non-vectorized implementation to test code.
def q_expectation_monte_carlo_dumb(variational_mu, variational_psi, X, T, prior_sigma_sq, nsamples):
  assert variational_mu.shape == variational_psi.shape
  C, D = variational_mu.shape
  N, _ = X.shape

  softmax_num = 0
  softmax_denom = 0
  prior   = 0
  entropy = 0

  for j in range(nsamples):
    W = (np.random.standard_normal(size=variational_mu.shape) * np.sqrt(np.exp(variational_psi))) + variational_mu

    for i in range(N):
      cls = T[i]
      softmax_num += np.dot(X[i], W[cls])
      S = 0
      for c in range(C):
        S += np.exp(np.dot(X[i], W[c]))
      softmax_denom -= np.log(S)

    for c in range(C):
      for d in range(D):
        prior   += norm_logpdf(W[c,d], 0, prior_sigma_sq)
        entropy -= norm_logpdf(W[c,d], variational_mu[c,d], np.exp(variational_psi[c,d]))

  return (softmax_num + softmax_denom + prior + entropy) / nsamples

def calc_grad(mu, psi, nsamples):
  grad_mu = 0.
  grad_psi = 0.

  for j in range(nsamples):
    W = (np.random.standard_normal(size=mu.shape) * np.sqrt(np.exp(psi))) + mu
    grad_mu += (W - mu) / np.exp(psi)
    grad_psi += (W - mu)**2 / (2*np.exp(psi)) - 0.5

  return (grad_mu / nsamples, grad_psi / nsamples)

def train(train_images, train_labels, C, D, nsamples, iterations, prior_sigma_sq, learning_rate, paramsfn=None):

  variational_mu  = np.random.standard_normal(size=(C, D))
  variational_psi = np.random.standard_normal(size=(C, D))

  def objective(params, iter):
   return -q_expectation_monte_carlo(params[0], params[1], train_images, train_labels, prior_sigma_sq, nsamples=nsamples)

  def callback(params, idx, g):
    mu, psi = params
    accuracy = compute_accuracy(train_images, train_labels, mu, psi, nsamples)
    expectation = objective((mu, psi), None)
    #print(idx, '%.3f' % accuracy, '%.3f' % expectation, sep='\t')

  grad = autograd.grad(objective)
  variational_mu, variational_psi = autograd.optimizers.adam(grad, (variational_mu, variational_psi), step_size=learning_rate, num_iters=iterations, callback=callback)
  if paramsfn is not None:
    np.savez(paramsfn, mu=variational_mu, psi=variational_psi)

  print('ELBO', objective((variational_mu, variational_psi), None))
  return (variational_mu, variational_psi)

def compute_accuracy(X, T, mu, psi, nsamples):
  N, D = X.shape
  C, _ = mu.shape

  acc = np.zeros(N)

  for j in range(nsamples):
    W = (np.random.standard_normal(size=mu.shape) * np.sqrt(np.exp(psi))) + mu
    XWt = np.dot(X, W.T) # NxC
    softmax_num = XWt[range(N),T]
    B = np.max(XWt, axis=1)
    softmax_denom = B + np.log(np.sum(np.exp(XWt - B[:,np.newaxis]), axis=1))
    acc += np.exp(softmax_num - softmax_denom)

  acc /= nsamples
  return np.mean(acc)

def save_images(prefix, variational_mu, variational_psi):
  W = (np.random.standard_normal(size=variational_mu.shape) * np.sqrt(np.exp(variational_psi))) + variational_mu
  data.save_images(variational_mu,  '%s.mu.png' % prefix)
  data.save_images(np.exp(variational_psi), '%s.sigma.png' % prefix)
  data.save_images(W, '%s.W.png' % prefix)

def main():
  np.random.seed(789)

  prior_sigma_sq = float(sys.argv[1])

  batch_size = 30
  nsamples = 100
  iterations = 2000
  #prior_sigma_sq = 1e-1
  learning_rate = 1e-1
  paramsfn = 'params.npz'

  N_data, train_images, train_labels, test_images, test_labels = data.load_mnist()
  train_images, train_labels = make_minibatch(train_images, train_labels, batch_size)

  _, C = train_labels.shape
  _, D = train_images.shape
  # Convert from one-hot encoding to vector.
  train_labels = np.argmax(train_labels, axis=1)
  test_labels  = np.argmax(test_labels, axis=1)

  if os.path.exists(paramsfn):
    params = np.load(paramsfn)
    variational_mu, variational_psi = params['mu'], params['psi']
  else:
    variational_mu, variational_psi = train(train_images, train_labels, C, D, nsamples, iterations, 10**prior_sigma_sq, learning_rate, paramsfn=None)
    print('train', prior_sigma_sq, compute_accuracy(train_images, train_labels, variational_mu, variational_psi, nsamples), sep='\t')
    print('test',  prior_sigma_sq, compute_accuracy(test_images,  test_labels,  variational_mu, variational_psi, nsamples), sep='\t')
    save_images('q2.%s' % prior_sigma_sq, variational_mu, variational_psi)
    print()

main()
