def partition_images(imgs, labels):
  C = labels.shape[1]
  partitioned = {}
  for c in range(C):
    partitioned[c] = imgs[labels[:,c] == 1]
  return partitioned
