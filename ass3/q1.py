from __future__ import print_function, division
import data
import numpy as np
import random
import common

np.set_printoptions(threshold=np.nan)

def make_minibatch(imgs, labels, size):
  assert len(imgs) == len(labels)
  N, _ = imgs.shape
  idxs = np.arange(N)
  np.random.shuffle(idxs)
  idxs = idxs[:size]
  return (imgs[idxs,:], labels[idxs,:])

def calc_llh_for_class(X, W, C, c):
  WXt = np.dot(W, X.T)
  # Find max in each column: Nx1
  B = np.max(WXt, axis=0)
  expWXt = np.exp(WXt - np.tile(B, (C, 1)))
  logsum = np.log(np.sum(expWXt, axis=0)) + B # Nx1

  wX = np.dot(X, W[c]) # Nx1
  wX -= logsum
  return wX

def calc_llh(W, imgs, labels):
  parted = common.partition_images(imgs, labels)
  C = len(parted)

  llh = 0
  for c, X in parted.items():
    llh += np.sum(calc_llh_for_class(X, W, C, c))
  return llh

def grad_desc(imgs, labels, sigma_sq=None):
  N, D = imgs.shape
  _, C = labels.shape
  W = np.random.normal(size=(C, D))
  W = np.zeros((C, D))
  batch_size = 100
  learn_rate = 0.5
  iterations = 1000

  last_llh = -float('inf')
  last_W = None

  for I in range(iterations):
    mb_imgs, mb_labels = make_minibatch(imgs, labels, batch_size)
    N_batch, _ = mb_imgs.shape
    parted = common.partition_images(mb_imgs, mb_labels)

    grad = np.zeros(W.shape)
    for c, X in parted.items():
      for x in X:
        Wx = np.dot(W, x)
        b = np.max(Wx)
        logsum = b + np.log(np.sum(np.exp(Wx - b)))
        sumc = np.exp(logsum)

        for k in range(C):
          if k == c:
            grad_wk = x * (1 - (np.exp(Wx[k]) / sumc))
          else:
            grad_wk = -x * np.exp(Wx[k]) / sumc
          # Regularize via MAP estimate, using N(0, sigma_sq) prior on the weights.
          if sigma_sq is not None:
              grad_wk -= W[k,:] / sigma_sq
          grad[k,:] += grad_wk

    grad /= N_batch
    W += learn_rate * grad

    llh = calc_llh(W, imgs, labels)
    if llh > last_llh:
      #print('%s\tkeeping\t%.2e\t%.2e' % (I, learn_rate, llh))
      last_llh = llh
      last_W = W
      learn_rate *= 1.1
    else:
      #print('%s\trevert\t%.2e\t%.2e' % (I, learn_rate, llh))
      llh = last_llh
      W = last_W
      learn_rate *= 0.5

  return W

def classify(X, C, W):
  N, D = X.shape
  cllh = np.zeros((N, C))
  for c in range(C):
    cllh[:,c] = calc_llh_for_class(X, W, C, c)
  return np.argmax(cllh, axis=1)

def softmax(X):
  b = np.max(X)
  logS = X - (b + np.log(np.sum(np.exp(X - b))))
  return np.exp(logS)

def evaluate(W, train_images, train_labels, test_images, test_labels):
  _, C = train_labels.shape
  vals = []
  for dataset, X, L in (('Training', train_images, train_labels), ('Test', test_images, test_labels)):
    N, D = X.shape
    llh = calc_llh(W, X, L)
    classes = np.argmax(L, axis=1)
    preds = classify(X, C, W)
    # Accuracy
    vals.append(np.sum(preds == classes) / N)
    # Mean predictive LLH
    vals.append(llh / N)
  return vals

def q1(train_images, train_labels, test_images, test_labels):
  W_mle = grad_desc(train_images, train_labels)
  print('MLE', *['%.2f' % V for V in evaluate(W_mle, train_images, train_labels, test_images, test_labels)], sep='\t')
  data.save_images(W_mle, 'q1.mle.weights.png')

  W_map = {}
  for order in np.arange(-1, 3, step=0.1):
    sigma_sq = 10**order
    W_map[order] = grad_desc(train_images, train_labels, sigma_sq=sigma_sq)
    data.save_images(W_map[order], 'q1.map.%s.weights.png' % order)
    print('MAP %s' % order, *['%.2f' % V for V in evaluate(W_map[order], train_images, train_labels, test_images, test_labels)], sep='\t')

def main():
  np.random.seed(456)
  N_data, train_images, train_labels, test_images, test_labels = data.load_mnist()
  train_images, train_labels = make_minibatch(train_images, train_labels, 30)
  q1(train_images, train_labels, test_images, test_labels)

main()
